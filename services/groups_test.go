package services_test

import (
	"fmt"
	"go-OSCP-Master-API/services"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

var (
	loginUrl    string
	validateUrl string
)

var (
	server    *httptest.Server
	reader    io.Reader //Ignore this for now
	serverUrl string
)

func MainTest_Init() {
	serverUrl = fmt.Sprintf("%s", server.URL)

}

func init() {
	router := mux.NewRouter()
	router.HandleFunc("/groups", services.GetGroupsbyProject)

	server = httptest.NewServer(router) //Creating new server with the user handlers

	MainTest_Init()
}

func TestGetGroupsbyProject_WithValidOSToken_ShouldReturnOK(t *testing.T) {
	OSCPtoken := "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjdy1wb3J0YWwtcGxnIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFwaS1hZG1pbi10b2tlbi1xMXAyaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhcGktYWRtaW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5YmRjNDRjMC0xODY2LTExZTctYmRiNS0wMDUwNTY4YzU3ODEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6Y3ctcG9ydGFsLXBsZzphcGktYWRtaW4ifQ.Lxz6OfrGe_7DaDWZfmpDwqgmcV0ON6wTCoyRJJJaH5O9TyHDyC2tAm-XB0CYue20U9ymBNFUE8XN9Gam1JUfYwesxNLhwzdwpgN-ML58_f_g6rL7ZmhWrS6GZVe8ajvlGsXdYTCaTCiT5Dct8wnkI7S8Hq7Vlu7IrctQvWwwYuLvQgpBf8B8-He98t5QmtN8SJJedEZGvQ7aJ_YOu8Ho9K0i4W-KCCX13FYHnxW4s0gtlMMKXg4pFCV1Vm6gK4TxdWiXI6Im13BzGza6XVz-A3OGNvI4Lbk54nY2PjhmJNQ9doagDYByuG2eQZY1PzYuKag_IMXsD7MKTOcUrdy8Pw"
	KcToken := "kctoken"
	//reader = strings.NewReader(clientData) //Convert string to reader

	//reader = strings.NewReader(clientData)

	request, err := http.NewRequest("POST", serverUrl+"/groups", nil) //Create request with JSON body
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Authorization", "Bearer "+KcToken)
	request.Header.Set("OscpToken", "Bearer "+OSCPtoken)

	res, err := http.DefaultClient.Do(request)

	if err != nil {
		t.Error(err) //Something is wrong while sending request
		t.Errorf("Failed.Success expected: %d", res.StatusCode)
	}

	if res.StatusCode != 200 {
		t.Errorf("Failed.Success expected: %d", res.StatusCode) //If not the result expected
		fmt.Println(request)
	}
}

func TestGetGroupsbyProject_WithInvalidOSToken_ShouldReturnError(t *testing.T) {
	OSCPtoken := "Invalid.eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjdy1wb3J0YWwtcGxnIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFwaS1hZG1pbi10b2tlbi1xMXAyaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhcGktYWRtaW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5YmRjNDRjMC0xODY2LTExZTctYmRiNS0wMDUwNTY4YzU3ODEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6Y3ctcG9ydGFsLXBsZzphcGktYWRtaW4ifQ.Lxz6OfrGe_7DaDWZfmpDwqgmcV0ON6wTCoyRJJJaH5O9TyHDyC2tAm-XB0CYue20U9ymBNFUE8XN9Gam1JUfYwesxNLhwzdwpgN-ML58_f_g6rL7ZmhWrS6GZVe8ajvlGsXdYTCaTCiT5Dct8wnkI7S8Hq7Vlu7IrctQvWwwYuLvQgpBf8B8-He98t5QmtN8SJJedEZGvQ7aJ_YOu8Ho9K0i4W-KCCX13FYHnxW4s0gtlMMKXg4pFCV1Vm6gK4TxdWiXI6Im13BzGza6XVz-A3OGNvI4Lbk54nY2PjhmJNQ9doagDYByuG2eQZY1PzYuKag_IMXsD7MKTOcUrdy8Pw"
	KcToken := "kctoken"

	request, err := http.NewRequest("POST", serverUrl+"/groups", nil) //Create request with JSON body
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Authorization", "Bearer "+KcToken)
	request.Header.Set("OscpToken", "Bearer "+OSCPtoken)

	res, err := http.DefaultClient.Do(request)

	if err != nil {
		t.Error(err) //Something is wrong while sending request
		t.Errorf("Failed.Success expected: %d", res.StatusCode)
	}

	if res.StatusCode == 200 {
		t.Errorf("Failed.Success expected: %d", res.StatusCode) //If not the result expected
		fmt.Println(request)
	}
}

func TestGetGroupsbyProject_WithNoOSToken_ShouldReturnError(t *testing.T) {
	KcToken := "kctoken"

	request, err := http.NewRequest("POST", serverUrl+"/groups", nil) //Create request with JSON body
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Authorization", "Bearer "+KcToken)

	res, err := http.DefaultClient.Do(request)

	if err != nil {
		t.Error(err) //Something is wrong while sending request
		t.Errorf("Failed.Success expected: %d", res.StatusCode)
	}

	if res.StatusCode == 200 {
		t.Errorf("Failed.Success expected: %d", res.StatusCode) //If not the result expected
		fmt.Println(request)
	}
}

func TestGetGroupsbyProject_WithNoKCToken_ShouldReturnError(t *testing.T) {
	OSCPtoken := "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjdy1wb3J0YWwtcGxnIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFwaS1hZG1pbi10b2tlbi1xMXAyaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhcGktYWRtaW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5YmRjNDRjMC0xODY2LTExZTctYmRiNS0wMDUwNTY4YzU3ODEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6Y3ctcG9ydGFsLXBsZzphcGktYWRtaW4ifQ.Lxz6OfrGe_7DaDWZfmpDwqgmcV0ON6wTCoyRJJJaH5O9TyHDyC2tAm-XB0CYue20U9ymBNFUE8XN9Gam1JUfYwesxNLhwzdwpgN-ML58_f_g6rL7ZmhWrS6GZVe8ajvlGsXdYTCaTCiT5Dct8wnkI7S8Hq7Vlu7IrctQvWwwYuLvQgpBf8B8-He98t5QmtN8SJJedEZGvQ7aJ_YOu8Ho9K0i4W-KCCX13FYHnxW4s0gtlMMKXg4pFCV1Vm6gK4TxdWiXI6Im13BzGza6XVz-A3OGNvI4Lbk54nY2PjhmJNQ9doagDYByuG2eQZY1PzYuKag_IMXsD7MKTOcUrdy8Pw"

	request, err := http.NewRequest("POST", serverUrl+"/groups", nil) //Create request with JSON body
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	request.Header.Set("OscpToken", "Bearer "+OSCPtoken)

	res, err := http.DefaultClient.Do(request)

	if err != nil {
		t.Error(err) //Something is wrong while sending request
		t.Errorf("Failed.Success expected: %d", res.StatusCode)
	}

	if res.StatusCode == 200 {
		t.Errorf("Failed.Success expected: %d", res.StatusCode) //If not the result expected
		fmt.Println(request)
	}
}

func TestGetGroupsbyProject_WithSelectorParam_ShouldReturnOK(t *testing.T) {
	OSCPtoken := "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjdy1wb3J0YWwtcGxnIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFwaS1hZG1pbi10b2tlbi1xMXAyaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhcGktYWRtaW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5YmRjNDRjMC0xODY2LTExZTctYmRiNS0wMDUwNTY4YzU3ODEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6Y3ctcG9ydGFsLXBsZzphcGktYWRtaW4ifQ.Lxz6OfrGe_7DaDWZfmpDwqgmcV0ON6wTCoyRJJJaH5O9TyHDyC2tAm-XB0CYue20U9ymBNFUE8XN9Gam1JUfYwesxNLhwzdwpgN-ML58_f_g6rL7ZmhWrS6GZVe8ajvlGsXdYTCaTCiT5Dct8wnkI7S8Hq7Vlu7IrctQvWwwYuLvQgpBf8B8-He98t5QmtN8SJJedEZGvQ7aJ_YOu8Ho9K0i4W-KCCX13FYHnxW4s0gtlMMKXg4pFCV1Vm6gK4TxdWiXI6Im13BzGza6XVz-A3OGNvI4Lbk54nY2PjhmJNQ9doagDYByuG2eQZY1PzYuKag_IMXsD7MKTOcUrdy8Pw"
	KcToken := "kctoken"
	//reader = strings.NewReader(clientData) //Convert string to reader

	//reader = strings.NewReader(clientData)

	request, err := http.NewRequest("GET", serverUrl+"/groups", nil) //Create request with JSON body
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Authorization", "Bearer "+KcToken)
	request.Header.Set("OscpToken", "Bearer "+OSCPtoken)

	res, err := http.DefaultClient.Do(request)

	if err != nil {
		t.Error(err) //Something is wrong while sending request
		t.Errorf("Failed.Success expected: %d", res.StatusCode)
	}

	if res.StatusCode != 200 {
		t.Errorf("Failed.Success expected: %d", res.StatusCode) //If not the result expected
		fmt.Println(request)
	}
}

/*
This is an example application to demonstrate parsing an ID Token.
*/
package main

import (
	"go-OSCP-Master-API/services"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {

	router := mux.NewRouter()

	//router.HandleFunc("/groups", services.GetGroups).Methods("GET")
	router.HandleFunc("/groups", services.CreateGroups).Methods("POST")
	router.HandleFunc("/groups/{GROUP}", services.DeleteGroup).Methods("DELETE")
	router.HandleFunc("/groups/{GROUP}", services.EditGroup).Methods("PUT")
	router.HandleFunc("/groups", services.GetGroupsbyProject).Methods("GET")
	router.HandleFunc("/groups", services.GetGroupsbyProject).Methods("GET").Queries("selector", "{selector}")
	router.HandleFunc("/groups/{GROUP}/roles", services.GetGroupRoles).Methods("GET")
	router.HandleFunc("/groups/{GROUP}/roles", services.GetGroupRoles).Methods("GET").Queries("roles", "{roles}") //TODO: can /roles be removed?

	router.HandleFunc("/user/{USER}", services.GetUserRoles).Methods("GET")
	router.HandleFunc("/user/{USER}", services.GetUserRoles).Methods("GET").Queries("roles", "{roles}")

	router.HandleFunc("/roles", services.GetAllRoles).Methods("GET")
	router.HandleFunc("/roles", services.GetAllRoles).Methods("GET").Queries("roles", "{roles}")

	//router.Queries("selector", "{selector}")
	//router.HandleFunc("/groupsselector", services.GetbyProject).Methods("GET")

	//router.HandleFunc("/groups/{GROUP}", services.GetGroup).Methods("GET")
	//router.HandleFunc("/groups/{GROUP}", services.AddLabel).Methods("PATCH")
	//router.HandleFunc("/groups/{GROUP}", services.AddUsersToGroup).Methods("PUT")
	//router.HandleFunc("/groups/{GROUP}/removeuser", services.RemoveUsersFromGroup).Methods("PUT")
	//router.HandleFunc("/validate", services.Validate)
	router.HandleFunc("/test", services.Test)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"*"},
	})

	handler := c.Handler(router)

	http.ListenAndServe(":8005", handler)
}

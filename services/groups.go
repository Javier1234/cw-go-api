package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
)

//Group is a portal app Group.
type Group struct {
	Name   string            `json:"name,omitempty"`
	Users  []string          `json:"users,omitempty"`
	Labels map[string]string `json:"labels,omitempty"`
}

//OSGroup is an Openshift Group. It may be contained in an Items[] struct or just come as a single group. I mixed both possibilities in a struct to handle them easier
type OSGroup struct {
	//Kind       string `json:"kind,omitempty"`
	//ApiVersion string `json:"apiVersion,omitempty"`
	Items []struct {
		Metadata struct {
			Name              string            `json:"name,omitempty"`
			UID               string            `json:"uid,omitempty"`
			ResourceVersion   string            `json:"resourceVersion,omitempty"`
			CreationTimestamp string            `json:"creationTimestamp,omitempty"`
			Labels            map[string]string `json:"labels,omitempty"`
		} `json:"metadata,omitempty"`
		Users []string `json:"users,omitempty"`
	} `json:"items,omitempty"`
	Metadata struct {
		Name              string            `json:"name,omitempty"`
		UID               string            `json:"uid,omitempty"`
		ResourceVersion   string            `json:"resourceVersion,omitempty"`
		CreationTimestamp string            `json:"creationTimestamp,omitempty"`
		Labels            map[string]string `json:"labels,omitempty"`
	} `json:"metadata,omitempty"`
	Users []string `json:"users,omitempty"`
}

/*
type OSProjectRoles struct {
	Items []struct {
		RoleBindings []struct {
			Name string `json:"name,omitempty"`
		} `json:"roleBindings,omitempty"`
	} `json:"items,omitempty"`
}
*/

//OSProjectRoleBinding is the whole structure of the openshift rolebinding for a namespace
type OSProjectRoleBinding struct {
	Metadata struct {
		Name              string            `json:"name,omitempty"`
		Namespace         string            `json:"namespace,omitempty"`
		UID               string            `json:"uid,omitempty"`
		ResourceVersion   string            `json:"resourceVersion,omitempty"`
		CreationTimestamp string            `json:"creationTimestamp,omitempty"`
		Labels            map[string]string `json:"labels,omitempty"`
	} `json:"metadata,omitempty"`
	UserNames  []string             `json:"userNames,omitempty"`
	GroupNames []string             `json:"groupNames"` //F1 : cannot omit this group when deleting role if the slice is empty
	Subjects   []RoleBindingSubject `json:"subjects,omitempty"`
	/*Subjects   []struct {
		Kind      string `json:"kind,omitempty"`
		Namespace string `json:"namespace,omitempty"`
		Name      string `json:"name,omitempty"`
	} `json:"subjects,omitempty"`*/
	RoleRef struct {
		Name string `json:"name,omitempty"`
	} `json:"roleRef,omitempty"`
}

//RoleBindingSubject is a substruct of OSProjectRoleBinding
type RoleBindingSubject struct {
	Kind      string `json:"kind,omitempty"`
	Namespace string `json:"namespace,omitempty"`
	Name      string `json:"name,omitempty"`
}

//Token should be a JWT-Token
type Token struct {
	Token string `json:"token,omitempty"`
}

//Tenant as it can be found in the Java-API
type Tenant struct {
	Name string `json:"name,omitempty"`
	ID   int    `json:"id,omitempty"`
}

var OSCPBaseURL = "https://manage.playground.itandtel.at"
var apiBaseURL = "https://api.playground.itandtel.at"

//var ServiceAccount = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJjdy1wb3J0YWwtcGxnIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFwaS1hZG1pbi10b2tlbi1xMXAyaCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJhcGktYWRtaW4iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI5YmRjNDRjMC0xODY2LTExZTctYmRiNS0wMDUwNTY4YzU3ODEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6Y3ctcG9ydGFsLXBsZzphcGktYWRtaW4ifQ.Lxz6OfrGe_7DaDWZfmpDwqgmcV0ON6wTCoyRJJJaH5O9TyHDyC2tAm-XB0CYue20U9ymBNFUE8XN9Gam1JUfYwesxNLhwzdwpgN-ML58_f_g6rL7ZmhWrS6GZVe8ajvlGsXdYTCaTCiT5Dct8wnkI7S8Hq7Vlu7IrctQvWwwYuLvQgpBf8B8-He98t5QmtN8SJJedEZGvQ7aJ_YOu8Ho9K0i4W-KCCX13FYHnxW4s0gtlMMKXg4pFCV1Vm6gK4TxdWiXI6Im13BzGza6XVz-A3OGNvI4Lbk54nY2PjhmJNQ9doagDYByuG2eQZY1PzYuKag_IMXsD7MKTOcUrdy8Pw"

var ServiceAccount = os.Getenv("MYSERVICEACCOUNT")

var OSCPRoles = []string{"developer", "operator", "oscpadmin", "deployer"}
var PortalRoles = []string{"billingadmin", "monitoring", "useradmin", "tenantadmin", "superadmin"}

func Test(w http.ResponseWriter, req *http.Request) {

	/*if Validate(w, req) {
		fmt.Println("DO CRUD")
	}*/

	//w.Header().Set("Content-Type", "application/json")
	//w.Write([]byte(ServiceAccountENV))

}

//If adminValidation true, the user must be an admin to pass the validation
func Validate(w http.ResponseWriter, req *http.Request, adminValidation bool) bool {

	//TESTCASES
	//user

	//reqToken := req.Header.Get("Authorization")
	//fmt.Println("token:>", reqToken)
	fmt.Println("Validating-------------------------")

	if ServiceAccount == "" {
		http.Error(w, "ServiceAcc environment var empty or invalid", 403)
		return false
	}
	KcTokenBearer := req.Header.Get("Authorization")

	KcToken := strings.TrimPrefix(KcTokenBearer, "Bearer ")

	token, err := jwt.Parse(KcToken, func(token *jwt.Token) (interface{}, error) {

		// Don't forget to validate the alg is what you expect:
		//	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		//	return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		//	}

		return []byte("9R9nzUPL4fqWhrRx"), nil
	})

	if token.Valid {
		fmt.Println("Provider Token validated")
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			http.Error(w, "That's not even a token", 403)
			return false
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			http.Error(w, "Token is expired", 403)
			return false
		}

	} //else {
	//fmt.Println("Couldn't handle this token:", err)
	//	return false
	//}

	//Find out which user am I from the Provider token's claims
	var user = ""
	var issuerApp = ""
	if claims, ok := token.Claims.(jwt.MapClaims); ok {

		if claims["email"] != nil {
			user = claims["email"].(string)
			issuerApp = claims["aud"].(string)
			fmt.Println("User retrieved from provider token's claim: " + user)
			fmt.Println("The app is: " + issuerApp)
		} else {
			//fmt.Println("Couldnt retrieve user. No user claim found ")
			http.Error(w, "Forbidden. Couldnt retrieve user. token claims invalid or not found", 401)
			return false
		}
	} else {
		fmt.Println(err)
		return false
	}

	//Find out which is the user's TenantID
	req, err = http.NewRequest("GET", apiBaseURL+"/myTenants", nil)
	req.Header.Set("Authorization", "Bearer "+KcToken)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		http.Error(w, "Could not retrieve tenant", 403)
		return false
	}

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	tenant := make([]Tenant, 0)
	json.Unmarshal([]byte(string(body)), &tenant)
	fmt.Println("tenant:>", tenant)
	fmt.Println(tenant[0].ID)
	//TODO: what if more tenants?

	//In case an endpoint is not only restricted to admins, we can return now
	if adminValidation == false {
		return true
	}

	//Find out if the user is an useradmin. Look for in the useradmin group of his Tenant namespace
	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)              //fmt.Println("response Body:", string(body))

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	//var GroupList []Group

	for _, aGroup := range osgroup.Items {

		//Check only the groups from the tenant. Look if the user is in the group. If yes, check if group is useradmin
		if strings.HasPrefix(aGroup.Metadata.Name, strconv.Itoa(tenant[0].ID)) {

			for _, aUser := range aGroup.Users {
				if user == aUser {

					for aKey, aValue := range aGroup.Metadata.Labels {
						if aKey == "useradmin" && aValue == issuerApp {
							fmt.Println("Validation Success. User found. User is useradmin: " + user)
							return true // TODO : what if apart from useradmin:portal, we want useradmin:anotherapp? we cant duplicate keys!
						}
					}
					fmt.Println(aGroup)
					break
				}
			}

		}

	}

	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+strconv.Itoa(tenant[0].ID)+"-tenantadmin", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	for _, v := range osgroup.Users {
		if user == v {
			fmt.Println("Validation Success. User found. User is tenantadmin: " + v)
			return true
		}

	}

	fmt.Println("User is not any admin.Forbidden.")
	http.Error(w, "User is not any admin.Forbidden.", 403)

	return false

}

/*
//If adminValidation true, the user must be an admin to pass the validation
func Validate(w http.ResponseWriter, req *http.Request, adminValidation bool) bool {

	//TESTCASES
	//user

	//reqToken := req.Header.Get("Authorization")
	//fmt.Println("token:>", reqToken)
	fmt.Println("Validating-------------------------")

	if ServiceAccount == "" {
		http.Error(w, "ServiceAcc environment var empty or invalid", 403)
		return false
	}
	KcTokenBearer := req.Header.Get("Authorization")

	KcToken := strings.TrimPrefix(KcTokenBearer, "Bearer ")

	token, err := jwt.Parse(KcToken, func(token *jwt.Token) (interface{}, error) {

		// Don't forget to validate the alg is what you expect:
		//	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		//	return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		//	}

		return []byte("9R9nzUPL4fqWhrRx"), nil
	})

	if token.Valid {
		fmt.Println("Provider Token validated")
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			http.Error(w, "That's not even a token", 403)
			return false
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			http.Error(w, "Token is expired", 403)
			return false
		}

	} //else {
	//fmt.Println("Couldn't handle this token:", err)
	//	return false
	//}

	//Find out which user am I from the Provider token's claims
	var user = ""
	if claims, ok := token.Claims.(jwt.MapClaims); ok {

		if claims["email"] != nil {
			user = claims["email"].(string)
			fmt.Println("User retrieved from provider token's claim: " + user)
		} else {
			//fmt.Println("Couldnt retrieve user. No user claim found ")
			http.Error(w, "Forbidden. Couldnt retrieve user. token claims invalid or not found", 401)
			return false
		}
	} else {
		fmt.Println(err)
		return false
	}

	//Find out which is the user's TenantID
	req, err = http.NewRequest("GET", apiBaseURL+"/myTenants", nil)
	req.Header.Set("Authorization", "Bearer "+KcToken)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		http.Error(w, "Could not retrieve tenant", 403)
		return false
	}

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	tenant := make([]Tenant, 0)
	json.Unmarshal([]byte(string(body)), &tenant)
	fmt.Println("tenant:>", tenant)
	//TODO: what if more tenants?

	//In case an endpoint is not only restricted to admins, we can return now
	if adminValidation == false {
		return true
	}

	//Find out if the user is an useradmin. Look for in the useradmin group of his Tenant namespace
	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+strconv.Itoa(tenant[0].ID)+"-useradmin", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	for _, v := range osgroup.Users {

		if user == v {
			fmt.Println("Validation Success. User found. User is useradmin: " + v)
			return true
		}

	}

	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+strconv.Itoa(tenant[0].ID)+"-tenantadmin", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	for _, v := range osgroup.Users {
		if user == v {
			fmt.Println("Validation Success. User found. User is tenantadmin: " + v)
			return true
		}

	}

	fmt.Println("User is not any admin.Forbidden.")
	http.Error(w, "User is not any admin.Forbidden.", 403)

	return false

}*/

func GetTenantID(w http.ResponseWriter, req *http.Request) string {
	KcTokenBearer := req.Header.Get("Authorization")

	KcToken := strings.TrimPrefix(KcTokenBearer, "Bearer ")
	//Find out which is the user's TenantID
	req, err := http.NewRequest("GET", apiBaseURL+"/myTenants", nil)
	req.Header.Set("Authorization", "Bearer "+KcToken)

	if err != nil {
		fmt.Println(err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		http.Error(w, "Could not retrieve tenant", 403)
	}

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	tenant := make([]Tenant, 0)
	json.Unmarshal([]byte(string(body)), &tenant)
	fmt.Println("tenant:>", tenant)

	return strconv.Itoa(tenant[0].ID)
}

/*
func Validate(w http.ResponseWriter, req *http.Request) bool {

	//TESTCASES
	//user

	//reqToken := req.Header.Get("Authorization")
	//fmt.Println("token:>", reqToken)
	fmt.Println("Validating-------------------------")
	KcTokenBearer := req.Header.Get("Authorization")

	KcToken := strings.TrimPrefix(KcTokenBearer, "Bearer ")

	token, err := jwt.Parse(KcToken, func(token *jwt.Token) (interface{}, error) {

		// Don't forget to validate the alg is what you expect:

		return []byte("9R9nzUPL4fqWhrRx"), nil
	})

	if token.Valid {
		fmt.Println("Provider Token validated")
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			http.Error(w, "That's not even a token", 403)
			return false
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			http.Error(w, "Token is expired", 403)
			return false
		}

	}

	//Find out which user am I from the Provider token's claims
	var user = ""
	if claims, ok := token.Claims.(jwt.MapClaims); ok {

		if claims["email"] != nil {
			user = claims["email"].(string)
			fmt.Println("User retrieved from provider token's claim: " + user)
		} else {
			//fmt.Println("Couldnt retrieve user. No user claim found ")
			http.Error(w, "Forbidden. Couldnt retrieve user. token claims invalid or not found", 401)
			return false
		}
	} else {
		fmt.Println(err)
		return false
	}

	//Find out which is the user's TenantID
	req, err = http.NewRequest("GET", apiBaseURL+"/myTenants", nil)
	req.Header.Set("Authorization", "Bearer "+KcToken)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		http.Error(w, "Could not retrieve tenant", 403)
		return false
	}

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	tenant := make([]Tenant, 0)
	json.Unmarshal([]byte(string(body)), &tenant)
	fmt.Println("tenant:>", tenant)
	//TODO: what if more tenants?

	//Find out if the user is an useradmin. Look for in the useradmin group of his Tenant namespace
	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+strconv.Itoa(tenant[0].ID)+"-useradmin", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	for _, v := range osgroup.Users {

		if user == v {
			fmt.Println("Validation Success. User found. User is useradmin: " + v)
			return true
		}

	}

	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+strconv.Itoa(tenant[0].ID)+"-tenantadmin", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return false
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	json.Unmarshal([]byte(string(body)), &osgroup)
	fmt.Println("osgroup:>", osgroup)

	for _, v := range osgroup.Users {
		if user == v {
			fmt.Println("Validation Success. User found. User is tenantadmin: " + v)
			return true
		}

	}

	fmt.Println("User is not any admin.Forbidden.")
	http.Error(w, "User is not any admin.Forbidden.", 403)

	return false

}*/

func GetGroupsbyProject(w http.ResponseWriter, r *http.Request) {

	fmt.Println("--------------GET : GETS A GROUP/GROUPS -------------------")
	//if Validate(w, r) {
	if Validate(w, r, true) {

		//tenant := make([]Tenant, 0)

		thisTenantID := GetTenantID(w, r)

		key := r.FormValue("selector")

		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups?labelSelector="+key, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)             //fmt.Println("response Body:", string(body))

		var osgroup OSGroup
		json.Unmarshal([]byte(string(body)), &osgroup)
		//fmt.Println("osgroup:>", osgroup)

		var GroupList []Group

		for _, v := range osgroup.Items {
			//fmt.Println(v.Metadata.Name)
			//fmt.Println(v.Metadata.Labels)
			//fmt.Println(v.Users)
			var newgroup Group
			newgroup.Name = v.Metadata.Name
			newgroup.Labels = v.Metadata.Labels
			newgroup.Users = v.Users

			if strings.HasPrefix(newgroup.Name, thisTenantID) {
				GroupList = append(GroupList, newgroup)
			}
		}

		//fmt.Println(GroupList)

		jsonStr, err := json.Marshal(GroupList)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(string(jsonStr))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonStr)

	}

}

func GetAllRoles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("--------------GET : GETs THE AVAILABLE ROLE LIST-------------------")

	//if Validate(w, r) {
	query := r.FormValue("roles")
	fmt.Println(query)

	var Roles = []string{}

	if query == "openshift" {
		Roles = OSCPRoles
	} else if query == "portal" {
		Roles = PortalRoles
	} else {
		Roles = OSCPRoles
		//for _, r := range PortalRoles {
		Roles = append(Roles, PortalRoles...)
		//}

	}

	jsonStr, err := json.Marshal(Roles)
	if err != nil {
		fmt.Println(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonStr)
	//}

}
func GetUserRoles(w http.ResponseWriter, r *http.Request) {

	fmt.Println("--------------GET : GET USER ROLES-------------------")
	//if Validate(w, r) {
	if Validate(w, r, false) {
		vars := mux.Vars(r)
		key := vars["USER"]

		query := r.FormValue("roles")
		fmt.Println(query)

		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups", nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)             //fmt.Println("response Body:", string(body))

		var osgroup OSGroup
		json.Unmarshal([]byte(string(body)), &osgroup)
		//fmt.Println("osgroup:>", osgroup)

		//var GroupList []Group

		userLabels := make(map[string]string)

		for _, aGroup := range osgroup.Items {

			//v.Metadata.Labels
			//v.Users

			for _, aUser := range aGroup.Users {
				if aUser == key {
					fmt.Println("add labels")
					for key, value := range aGroup.Metadata.Labels {
						userLabels[key] = value
					}
				}
			}

		}

		var LabelsList []string

		for key, value := range userLabels {
			if strings.HasPrefix(key, "prj-") && (query == "openshift" || query == "") {
				//LabelsList = append(LabelsList, value)
				if len(LabelsList) == 0 {
					LabelsList = append(LabelsList, value)
				}
				//Find if duplicated
				for i, aRole := range LabelsList {
					if aRole == value {
						// Found!
						break
					} else if i == len(LabelsList)-1 {
						LabelsList = append(LabelsList, value)
					}
				}
			} else if key != "tenantid" && (value == query || query == "") {
				if len(LabelsList) == 0 {
					LabelsList = append(LabelsList, key)
				}
				//Find if duplicated
				for i, aRole := range LabelsList {
					if aRole == key {
						// Found!
						break
					} else if i == len(LabelsList)-1 {
						LabelsList = append(LabelsList, key)
					}
				}

			}

		}

		fmt.Println(LabelsList)

		jsonStr, err := json.Marshal(LabelsList)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(string(jsonStr))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonStr)

		//fmt.Println(userLabels)
	}
}

func GetGroupRoles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("--------------GET : GETS A GROUP's ROLES -------------------")
	if Validate(w, r, true) {
		//if Validate(w, r) {
		vars := mux.Vars(r)
		key := vars["GROUP"]

		query := r.FormValue("roles")
		fmt.Println(query)

		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key+"?roles="+query, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)             //fmt.Println("response Body:", string(body))

		var osgroup OSGroup
		json.Unmarshal([]byte(string(body)), &osgroup)
		fmt.Println("osgroup:>", osgroup)

		var LabelsList []string

		for key, value := range osgroup.Metadata.Labels {
			if strings.HasPrefix(key, "prj-") && (query == "openshift" || query == "") {
				//LabelsList = append(LabelsList, value)
				if len(LabelsList) == 0 {
					LabelsList = append(LabelsList, value)
				}
				//Find if duplicated
				for i, aRole := range LabelsList {
					if aRole == value {
						// Found!
						break
					} else if i == len(LabelsList)-1 {
						LabelsList = append(LabelsList, value)
					}
				}
			} else if key != "tenantid" && (value == query || query == "") {
				if len(LabelsList) == 0 {
					LabelsList = append(LabelsList, key)
				}
				//Find if duplicated
				for i, aRole := range LabelsList {
					if aRole == key {
						// Found!
						break
					} else if i == len(LabelsList)-1 {
						LabelsList = append(LabelsList, key)
					}
				}

			}

		}

		fmt.Println(LabelsList)

		jsonStr, err := json.Marshal(LabelsList)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(string(jsonStr))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonStr)
	}
}

func CreateGroups(w http.ResponseWriter, r *http.Request) {

	if Validate(w, r, true) {
		//if Validate(w, r) {
		fmt.Println("--------------POST : CREATE NEW GROUP -------------------")

		var newgroup Group
		_ = json.NewDecoder(r.Body).Decode(&newgroup)
		fmt.Println("newgroup:>", newgroup)

		go AddGroupToRolebindings(w, r, newgroup)

		//The Openshift-API requires the values inside the metadata field. Need to use OSGroup
		//osgroup := &OSGroup{}

		var osgroup OSGroup
		osgroup.Metadata.Name = newgroup.Name
		osgroup.Metadata.Labels = newgroup.Labels
		osgroup.Users = newgroup.Users

		jsonStr, err := json.Marshal(osgroup)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(string(jsonStr))

		req, err := http.NewRequest("POST", OSCPBaseURL+"/oapi/v1/groups", bytes.NewBuffer(jsonStr))
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("- POST group response Status:", resp.StatusCode)
		//fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)
		//fmt.Println("response Body:", string(body))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
			w.Header().Set("Content-Type", "application/json")
			w.Write(body)

		} else {
			w.WriteHeader(201)
		}

		//w.Header().Set("Content-Type", "application/json")
		//w.Write(body)

	}
}

func EditGroup(w http.ResponseWriter, r *http.Request) {
	//TESTCASES
	//Rolebinding with empty Group's array -> gets new group -> OK
	//Rolebinding with non-empty Group's array -> gets new group -> OK
	//Rolebinding with non-empty Group's array -> gets a group removed
	//Rolebinding with non-empty Group's array -> gets a group removed -> Rolebinding Group's array empty -> OK

	//Group with empty labels get new labels -> OK
	// --> one of the labels'role doesn't exist -> gets created -> OK
	// --> label's role exists -> gets created -> OK
	//Group with empty labels gets no new labels
	//Group with non-empty labels gets some labels updated -> Rolebinding erases group in old role and adds group in the new one -> OK
	//Group with non-empty labels gets all labels removed -> OK

	//Add new users -> OK
	//Remove users -> OK
	//Empty the user array -> OK

	//KcToken := r.Header.Get("Authorization")
	//OSCPtoken := r.Header.Get("OscpToken") //fmt.Println(r.Header.Get("Authorization"))

	fmt.Println("--------------PUT : EDIT GROUP -------------------")
	if Validate(w, r, true) {
		//if Validate(w, r) {
		vars := mux.Vars(r)
		key := vars["GROUP"]

		var updategroup Group
		_ = json.NewDecoder(r.Body).Decode(&updategroup)
		fmt.Println("the update -> ", updategroup)

		//check if tenantid exists, otherwise abort
		if _, ok := updategroup.Labels["tenantid"]; ok {
			fmt.Println("Check: tenantid exists in requested group")
		} else {
			http.Error(w, "Tenantid missing from group", 400)
			return
		}

		//find the group to update
		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		//fmt.Println("response Status:", resp.StatusCode)
		//fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)

		osgroup := &OSGroup{}
		json.Unmarshal([]byte(string(body)), &osgroup)
		fmt.Println("osgroup before edit:>", osgroup)

		//check if tenantid exists, otherwise abort
		if _, ok := osgroup.Metadata.Labels["tenantid"]; ok {
			fmt.Println("Check: tenantid exists in target group")
		} else {
			http.Error(w, "Tenantid missing from found group", 406)
			return
		}

		fmt.Println("Processing group label actions...")

		//F2 : take into account that the min length is 1 cause tenantid
		if (len(osgroup.Metadata.Labels) > 1) && len(updategroup.Labels) > 1 {

			var labelsToDelete = make(map[string]string)
			var labelsToCreate = make(map[string]string)

			fmt.Println("Checking removals...")
			var index1 = 0
			for key1, oldLabel := range osgroup.Metadata.Labels {
				index1 = 0
				for key2, newLabel := range updategroup.Labels {

					if key1 == key2 && newLabel == oldLabel {
						fmt.Println("*stays ", key1, key2)
						//fmt.Println("stays", index1, index2)
						//index1++
						break
					} else if index1 == len(updategroup.Labels)-1 { //if to the end of array
						labelsToDelete[key1] = oldLabel
						fmt.Println("*remove ", key1)
						//index1++
					}
					index1++
				}
			}

			var toDeleteGroup OSGroup
			toDeleteGroup.Metadata.Name = osgroup.Metadata.Name
			toDeleteGroup.Metadata.Labels = labelsToDelete
			//fmt.Println(toDeleteGroup)
			go RemoveGroupFromRolebindings(w, r, toDeleteGroup)

			fmt.Println("Checking adds...")
			var index2 = 0
			for key2, newLabel := range updategroup.Labels {
				index2 = 0
				for key1, oldLabel := range osgroup.Metadata.Labels {

					if key1 == key2 && newLabel == oldLabel {
						fmt.Println("*stays ", key1, key2)
						//fmt.Println("stays", index1, index2)
						//index2++
						break
					} else if index2 == len(osgroup.Metadata.Labels)-1 { //if to the end of array
						labelsToCreate[key2] = newLabel
						fmt.Println("add ", key2)
						//	index2++
					}
					index2++
				}
			}
			var toAddGroup Group
			toAddGroup.Name = osgroup.Metadata.Name
			toAddGroup.Labels = labelsToCreate
			//fmt.Println(toAddGroup)
			go AddGroupToRolebindings(w, r, toAddGroup)

		} else if (len(osgroup.Metadata.Labels) > 1) && len(updategroup.Labels) == 1 {

			fmt.Println("Update group labels empty, just remove all labels...")
			var toDeleteGroup OSGroup
			toDeleteGroup.Metadata.Name = osgroup.Metadata.Name
			toDeleteGroup.Metadata.Labels = osgroup.Metadata.Labels
			//fmt.Println(toDeleteGroup)
			go RemoveGroupFromRolebindings(w, r, toDeleteGroup)

		} else if (len(osgroup.Metadata.Labels) == 1) && len(updategroup.Labels) > 1 {
			fmt.Println("Original group labels empty, just add all labels...")
			var toAddGroup Group
			toAddGroup.Name = osgroup.Metadata.Name
			toAddGroup.Labels = updategroup.Labels
			//fmt.Println(toAddGroup)
			go AddGroupToRolebindings(w, r, toAddGroup)

		} else {
			fmt.Println("Both group labels empty, dont do anything...")
		}

		//Handle the Group update
		osgroup.Users = updategroup.Users
		osgroup.Metadata.Labels = updategroup.Labels

		fmt.Println("osgroup after edit:>", osgroup)

		jsonStr, err := json.Marshal(osgroup)
		if err != nil {
			fmt.Println(err)
			return
		}
		//fmt.Println(string(jsonStr))

		req, err = http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/groups/"+key, bytes.NewBuffer(jsonStr))
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client = &http.Client{}
		resp, err = client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.StatusCode)
		//fmt.Println("response Headers:", resp.Header)
		//body, _ = ioutil.ReadAll(resp.Body)

		//fmt.Println("response Body:", string(body))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)

		}

		//w.Header().Set("Content-Type", "application/json")
		//w.Write(body)
	}

}

func DeleteGroup(w http.ResponseWriter, r *http.Request) {

	fmt.Println("--------------DELETE : DELETE GROUP -------------------")

	//if Validate(w, r) {
	if Validate(w, r, true) {
		vars := mux.Vars(r)
		key := vars["GROUP"]
		//fmt.Println(w, "Key: "+key)

		//Getting group for rolebindings
		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(req.Method, err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("Get -> group to delete. Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)

		body, _ := ioutil.ReadAll(resp.Body) //fmt.Println("response Body:", string(body))

		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
			w.Header().Set("Content-Type", "application/json")
			w.Write(body)
			return
		}

		var osgroup OSGroup
		json.Unmarshal([]byte(string(body)), &osgroup)
		fmt.Println("osgroup to delete -> ", osgroup)

		//The rolebindings must be dealt with before deleting the group, otherwise the group labels cant be found anymore
		go RemoveGroupFromRolebindings(w, r, osgroup)

		req, err = http.NewRequest("DELETE", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(req.Method, err)
			return
		}

		client = &http.Client{}
		resp, err = client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("DEL -> group. Status: ", resp.StatusCode)
		//fmt.Println("response Headers:", resp.Header)
		body, _ = ioutil.ReadAll(resp.Body)

		//fmt.Println("response Body:", string(body))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)
			//w.Header().Set("Content-Type", "application/json")
			//w.Write(body)
			return
		}

		//w.Header().Set("Content-Type", "application/json")
		//w.Write(body)
	}

}

/*
func DeleteGroup(w http.ResponseWriter, r *http.Request) {

	//KcToken := r.Header.Get("Authorization")
	//OSCPtoken := r.Header.Get("OscpToken") //fmt.Println(r.Header.Get("Authorization"))

	if Validate(w, r) {

		vars := mux.Vars(r)
		key := vars["GROUP"]
		//fmt.Println(w, "Key: "+key)

		req, err := http.NewRequest("DELETE", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)

		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		fmt.Println("response Status:", resp.StatusCode)
		//fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)

		//fmt.Println("response Body:", string(body))

		//w.WriteHeader(resp.StatusCode)
		if resp.StatusCode < 200 || resp.StatusCode > 299 {
			w.WriteHeader(resp.StatusCode)

		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
	}

}*/

func GetProjectRolebindings(w http.ResponseWriter, r *http.Request) {

	//KcToken := r.Header.Get("Authorization")
	//OSCPtoken := r.Header.Get("OscpToken") //fmt.Println(r.Header.Get("Authorization"))

	vars := mux.Vars(r)

	key := vars["GROUP"]

	key = "1-label-test"
	//fmt.Println(w, "Key: "+key)

	req, err := http.NewRequest("GET", OSCPBaseURL+"oapi/v1/namespaces/"+key+"/policybindings", nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println("response Body:", string(body))

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)

}

func AddGroupToRolebindings(w http.ResponseWriter, r *http.Request, newgroup Group) {

	//ROLEBINDING
	//For every role of every project(label) of this new group, check if the role exists, then PUT or POST the role
	fmt.Println("----------Processing rolebindings group creation---------------")
	fmt.Println("The labels to add -> ", newgroup)

	for key, value := range newgroup.Labels {
		fmt.Println(">Iteration:   ", "key:", key, "value:", value)

		if key == "tenantid" {
			fmt.Println("- Skipping label tenantid...")
			continue
		}
		//TODO: What if the label is something else? not tenantid and not prj

		project := strings.TrimPrefix(key, "prj-")
		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)
		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		oldRolesbinding := &OSProjectRoleBinding{}
		json.Unmarshal([]byte(string(body)), &oldRolesbinding)
		fmt.Println("- pre-update Rolebinding -> ", oldRolesbinding)

		//To update the rolebinding with a new group, just pass the old rolebindng object and add the new group to GroupNames. The Subject will be automatically generated by Openshift
		if resp.StatusCode == 200 {
			fmt.Println("- Role found...")
			updateRolesbinding := oldRolesbinding

			fmt.Println("- Array length: " + strconv.Itoa(len(updateRolesbinding.GroupNames)))

			//TEST:If GroupNames has no groups, it wont enter the for loop. thats why  i had to duplicate the branch in an if.
			if len(updateRolesbinding.GroupNames) != 0 {
				//Check if the project is not already there
				for index, aName := range updateRolesbinding.GroupNames {

					if aName == newgroup.Name {
						fmt.Println("- Group already exists, therefore not doing anything...")
						break

					} else if newgroup.Name != project && index == len(updateRolesbinding.GroupNames)-1 {
						fmt.Println("- Updating group in rolebinding...")
						updateRolesbinding.GroupNames = append(updateRolesbinding.GroupNames, newgroup.Name)
						//fmt.Println(updateRolesbinding)

						jsonStr, err := json.Marshal(updateRolesbinding)
						if err != nil {
							fmt.Println(err)
							return
						}
						fmt.Println(string(jsonStr))

						req, err := http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, bytes.NewBuffer(jsonStr))
						req.Header.Set("Authorization", "Bearer "+ServiceAccount)

						if err != nil {
							fmt.Println(err)
							return
						}

						client := &http.Client{}
						resp, err := client.Do(req)
						if err != nil {
							panic(err)
						}
						defer resp.Body.Close()
						//body, _ := ioutil.ReadAll(resp.Body)
						if resp.StatusCode < 200 || resp.StatusCode > 299 {
							//w.WriteHeader(resp.StatusCode)

						}
						fmt.Println("- Rolebinding update response Status:", resp.StatusCode)

						//w.Header().Set("Content-Type", "application/json")
						//w.Write(body)
					}
				}
			} else {
				fmt.Println("- Using second branch...")
				fmt.Println("- Updating group in rolebinding...")
				updateRolesbinding.GroupNames = append(updateRolesbinding.GroupNames, newgroup.Name)
				fmt.Println("- updated rolebinding -> ", updateRolesbinding)

				jsonStr, err := json.Marshal(updateRolesbinding)
				if err != nil {
					fmt.Println(err)
					return
				}
				//fmt.Println(string(jsonStr))

				req, err := http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, bytes.NewBuffer(jsonStr))
				req.Header.Set("Authorization", "Bearer "+ServiceAccount)

				if err != nil {
					fmt.Println(err)
					return
				}

				client := &http.Client{}
				resp, err := client.Do(req)
				if err != nil {
					panic(err)
				}
				defer resp.Body.Close()
				//body, _ := ioutil.ReadAll(resp.Body)
				if resp.StatusCode < 200 || resp.StatusCode > 299 {
					//w.WriteHeader(resp.StatusCode)

				}
				fmt.Println("- Rolebinding update response Status:", resp.StatusCode)

				//w.Header().Set("Content-Type", "application/json")
				//w.Write(body)
			}

		} else {
			fmt.Println("- Role doesn't exist...")

			newRolesbinding := &OSProjectRoleBinding{}
			newRolesbinding.Metadata.Name = value
			newRolesbinding.Metadata.Namespace = project
			newRolesbinding.Metadata.CreationTimestamp = ""
			newRolesbinding.GroupNames = append(newRolesbinding.GroupNames, newgroup.Name)
			var subject RoleBindingSubject
			subject.Kind = "Group"
			subject.Name = project
			newRolesbinding.Subjects = append(newRolesbinding.Subjects, subject)
			newRolesbinding.RoleRef.Name = value
			fmt.Println(newRolesbinding)

			jsonStr, err := json.Marshal(newRolesbinding)
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println(string(jsonStr))

			req, err := http.NewRequest("POST", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings", bytes.NewBuffer(jsonStr))
			req.Header.Set("Authorization", "Bearer "+ServiceAccount)

			if err != nil {
				fmt.Println(err)
				return
			}

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()
			//body, _ := ioutil.ReadAll(resp.Body)
			if resp.StatusCode < 200 || resp.StatusCode > 299 {
				//w.WriteHeader(resp.StatusCode)

			}

			fmt.Println("-response Status:", resp.StatusCode)

		}

	}

}

func RemoveGroupFromRolebindings(w http.ResponseWriter, r *http.Request, osgroup OSGroup) {

	fmt.Println("---------- Processing rolebindings group deletion ---------------")
	fmt.Println("The labels to remove -> ", osgroup)

	for key, value := range osgroup.Metadata.Labels {
		fmt.Println("Iteration:   ", "Key:", key, "Value:", value)

		if key == "tenantid" {
			fmt.Println("- Skipping label tenantid...")
			continue
		}

		project := strings.TrimPrefix(key, "prj-")
		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)
		if err != nil {
			fmt.Println(req.Method, err)
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		osprojectRolesbinding := &OSProjectRoleBinding{}
		json.Unmarshal([]byte(string(body)), &osprojectRolesbinding)
		fmt.Println("- rolebinding struct ->  ", osprojectRolesbinding)

		if resp.StatusCode == 200 {
			fmt.Println("- Role found...")
			updateRolesbinding := osprojectRolesbinding

			//Check if the project is not already there
			for index, aName := range updateRolesbinding.GroupNames {
				if aName == osgroup.Metadata.Name {
					fmt.Println("- Group found in rolebinding, deleting it...")
					if len(updateRolesbinding.GroupNames) == 1 {

						updateRolesbinding.GroupNames = make([]string, 0) //F1 : if the slice has to be sent empty, the JSON should be [] in order to work in Openshift, cant be null or anything else. See https://danott.co/posts/json-marshalling-empty-slices-to-empty-arrays-in-go.html

					} else {
						updateRolesbinding.GroupNames = append(updateRolesbinding.GroupNames[:index], updateRolesbinding.GroupNames[index+1:]...)
					}

					//fmt.Println(updateRolesbinding)
					jsonStr, err := json.Marshal(updateRolesbinding)
					if err != nil {
						fmt.Println(err)
						return
					}
					//fmt.Println(string(jsonStr))

					req, err := http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, bytes.NewBuffer(jsonStr))
					req.Header.Set("Authorization", "Bearer "+ServiceAccount)

					client := &http.Client{}
					resp, err := client.Do(req)
					if err != nil {

						panic(err)
					}
					defer resp.Body.Close()
					//body, _ := ioutil.ReadAll(resp.Body)
					if resp.StatusCode < 200 || resp.StatusCode > 299 {
						//	w.WriteHeader(resp.StatusCode)

					}

					fmt.Println("- PUT -> updated group. Status: ", resp.StatusCode)
					if err != nil {
						fmt.Println(req.Method, err)
						return
					}

					//w.Header().Set("Content-Type", "application/json")
					//	w.Write(body)

					break

				} else if aName != osgroup.Metadata.Name && index == len(updateRolesbinding.GroupNames)-1 {
					fmt.Println("- Strange, something is wrong, group not found...")
				}

			}

		} else {
			fmt.Println("- Strange, something is wrong, role not found...")
		}

	}

}

/*
func RemoveGroupFromRolebindings(w http.ResponseWriter, r *http.Request, key string) {

	fmt.Println("----------Processing rolebindings deletion---------------")

	req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
	req.Header.Set("Authorization", "Bearer "+ServiceAccount)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)             //fmt.Println("response Body:", string(body))

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	fmt.Println("osgroup:>", osgroup)

	for key, value := range osgroup.Metadata.Labels {
		fmt.Println("Iteration:   ", "Key:", key, "Value:", value)

		if key == "tenantid" {
			fmt.Println("- Skipping label tenantid...")
			continue
		}

		project := strings.TrimPrefix(key, "prj-")
		req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, nil)
		req.Header.Set("Authorization", "Bearer "+ServiceAccount)
		if err != nil {
			fmt.Println(err)
			return
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)

		osprojectRolesbinding := &OSProjectRoleBinding{}
		json.Unmarshal([]byte(string(body)), &osprojectRolesbinding)
		fmt.Println("- osprojectRolesbinding:>", osprojectRolesbinding)

		if resp.StatusCode == 200 {
			fmt.Println("- Role found...")
			updateRolesbinding := osprojectRolesbinding

			//Check if the project is not already there
			for index, aName := range updateRolesbinding.GroupNames {
				if aName == osgroup.Metadata.Name {
					fmt.Println("- Group found in rolebinding, deleting it...")
					if len(updateRolesbinding.GroupNames) == 1 {

						updateRolesbinding.GroupNames = make([]string, 0) //F1 : if the slice has to be sent empty, the JSON should be [] in order to work in Openshift, cant be null or anything else. See https://danott.co/posts/json-marshalling-empty-slices-to-empty-arrays-in-go.html

					} else {
						updateRolesbinding.GroupNames = append(updateRolesbinding.GroupNames[:index], updateRolesbinding.GroupNames[index+1:]...)
					}

					//fmt.Println(updateRolesbinding)
					jsonStr, err := json.Marshal(updateRolesbinding)
					if err != nil {
						fmt.Println(err)
						return
					}
					//fmt.Println(string(jsonStr))

					req, err := http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/namespaces/"+project+"/rolebindings/"+value, bytes.NewBuffer(jsonStr))
					req.Header.Set("Authorization", "Bearer "+ServiceAccount)

					fmt.Println("-response Status:", resp.StatusCode)
					if err != nil {
						fmt.Println(err)
						return
					}

					client := &http.Client{}
					resp, err := client.Do(req)
					if err != nil {
						panic(err)
					}
					defer resp.Body.Close()
					//body, _ := ioutil.ReadAll(resp.Body)
					if resp.StatusCode < 200 || resp.StatusCode > 299 {
						//	w.WriteHeader(resp.StatusCode)

					}

					//w.Header().Set("Content-Type", "application/json")
					//	w.Write(body)
					break

				} else if aName != osgroup.Metadata.Name && index == len(updateRolesbinding.GroupNames)-1 {
					fmt.Println("- Strange, something is wrong, group not found...")
				}

			}

		} else {
			fmt.Println("- Strange, something is wrong, role not found...")
		}

	}

}*/

API-Endpoints:

Validation:
Before executing an endpoint there is a validation performed against the Java-API (Keycloak-user exists in a Tenant) and Openshift (is tenantadmin/useradmin). The following endpoints don�t implement this validation:

/user/{USER} -> partial validation. Only validated against the Java-API
/roles -> No validation at all   

Headers:
Authorization -> Bearer {KeycloakToken}
Note:  At the moment I don�t need the OSCP-Token for anything, as we already spoke last day.

GET  -> /groups
                /groups?selector={label}
Success: 200

->	/user/{USER}     -> return all user roles
->	/user/{USER}?roles=openshift   -> return all openshift roles for the user
->	/user/{USER}?roles=portal   -> return all portal roles for the user

->	/groups/{GROUP}/roles     -> return all roles in the group
->	/groups/{GROUP}/roles?roles=openshift   -> return all openshift roles in the group
->	/groups/{GROUP}/roles?roles =portal   -> return all portal roles in the group

->	/roles     -> return a list with all available roles (openshift + portal)
->	/roles?roles=openshift   -> return a list with all openshift roles available
->	/roles?roles=portal   -> return a list with all portal roles available




POST -> /groups
Creates a new group and assigns the Group to every label�s rolebinding
Success: 201

Body example:
{
                "name":"MyGroupi",
                "labels": {
                "tenantid": "22",
                "prj-1-label-test": "view",
                "prj-1-mpaytest2": "edit"
                   },
                "Users":["testuser1","testuser2"]
}

PUT ->  /groups/{GroupName}
Updates selected group. Assign the Group to every new label�s rolebinding and deletes it from the removed labels�s rolebindings
Success: 200


Body example:
{
                "labels": {
                               "tenantid": "22",
                                "prj-1-label-test": "view",
                                "prj-1-mpaytest2": "view"
                                 },
                "Users":[ testuser1","testuser2","testuser3"]    
}

DEL -> /groups/{GroupName}
Deletes selected group. Removes the group from every label�s rolebinding
Success: 200

/*
func Validate(w http.ResponseWriter, req *http.Request) {

	reqToken := req.Header.Get("Authorization")
	//fmt.Println("token:>", reqToken)

	req, err := http.NewRequest("GET", apiBaseURL+"/myTenants", nil)
	req.Header.Set("Authorization", reqToken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	var tenant Tenant

	//_ = json.NewDecoder(resp.Body).Decode(&tenant)
	//fmt.Println("tenant:>", tenant)

	//The response from Java API sends a JSON but between brackets [{..json...}]. Because of these brackets, decode doesnt work for a struct. Need to trim them before dealing with the JSON
	var stringBody = string(body)
	cleanJSON := stringBody[1:(len(stringBody) - 1)] //fmt.Println("tenant:>", myJSON)
	bytesJSON := []byte(cleanJSON)
	json.Unmarshal(bytesJSON, &tenant)
	//fmt.Println("tenant:>", tenant)

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	//TODO: Now I should call the openshift api to see if the user exists in tehe tenantadmin or userandmin groups with the tenant id
	//Do this with Service Account and just search for the name which we received in the token,
	req, err = http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups", nil)
	req.Header.Set("Authorization", "Bearer "+OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client = &http.Client{}
	resp2, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp2.Body.Close()

	fmt.Println("response Status:", resp2.StatusCode)
	body2, _ := ioutil.ReadAll(resp2.Body)
	fmt.Println(string(body2))
	var osgroup OSGroup
	//_ = json.NewDecoder(resp2.Body).Decode(&osgroup)

	json.Unmarshal([]byte(string(body2)), &osgroup)
	fmt.Println("osgroup:>", osgroup)

	w.Header().Set("Content-Type", "application/json")
	w.Write(body2)

	//tentantJSON, _ := json.Marshal(tenant)
	//w.Header().Set("Content-Type", "application/json")
	//w.Write([]byte(tentantJSON))

}

*/
func GetGroup(w http.ResponseWriter, r *http.Request) {

	//KcToken := r.Header.Get("Authorization")
	OSCPtoken := r.Header.Get("OscpToken") //fmt.Println(r.Header.Get("Authorization"))

	//fmt.Println(r.Header.Get("OscpToken"))

	vars := mux.Vars(r)
	key := vars["GROUP"]
	//fmt.Println(w, "Key: "+key)

	req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	//TODO: Use this to return the values we need
	/*
		var osgroup OSGroup
		json.Unmarshal([]byte(string(body)), &osgroup)
		fmt.Println("osgroup:>", osgroup)*/

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)

}

func GetGroups(w http.ResponseWriter, r *http.Request) {

	//KcToken := r.Header.Get("Authorization")
	OSCPtoken := r.Header.Get("OscpToken") //fmt.Println(r.Header.Get("Authorization"))

	req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups", nil)
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode) //fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)             //fmt.Println("response Body:", string(body))

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	//fmt.Println("osgroup:>", osgroup)

	var GroupList []Group

	for _, v := range osgroup.Items {
		//fmt.Println(v.Metadata.Name)
		//fmt.Println(v.Metadata.Labels)
		//fmt.Println(v.Users)
		var newgroup Group
		newgroup.Name = v.Metadata.Name
		newgroup.Labels = v.Metadata.Labels
		newgroup.Users = v.Users
		GroupList = append(GroupList, newgroup)

	}

	//fmt.Println(GroupList)

	jsonStr, err := json.Marshal(GroupList)
	if err != nil {
		fmt.Println(err)
		return
	}
	//fmt.Println(string(jsonStr))

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonStr)

}

func AddLabel(w http.ResponseWriter, r *http.Request) {
	OSCPtoken := r.Header.Get("Authorization")
	vars := mux.Vars(r)
	key := vars["GROUP"]

	var newgroup Group
	_ = json.NewDecoder(r.Body).Decode(&newgroup)
	fmt.Println("newgroup:>", newgroup)

	osgroup := &OSGroup{}
	//osgroup.Metadata.Labels.Key = newgroup.KEY

	jsonStr, err := json.Marshal(osgroup)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonStr))

	req, err := http.NewRequest("PATCH", OSCPBaseURL+"/oapi/v1/groups/"+key, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	//fmt.Println("response Body:", string(body))

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)

}

func AddUsersToGroup(w http.ResponseWriter, r *http.Request) {

	OSCPtoken := r.Header.Get("Authorization")
	vars := mux.Vars(r)
	key := vars["GROUP"]

	var newgroup Group
	_ = json.NewDecoder(r.Body).Decode(&newgroup)
	fmt.Println("newgroup:>", newgroup)

	req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	fmt.Println("osgroup:>", osgroup)

	for _, element := range newgroup.Users {
		osgroup.Users = append(osgroup.Users, element)
	} //TODO: can be Repeated elements???

	fmt.Println("osgroup:>", osgroup)

	jsonStr, err := json.Marshal(osgroup)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonStr))

	req, err = http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/groups/"+key, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)

	//fmt.Println("response Body:", string(body))

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)

}

func RemoveUsersFromGroup(w http.ResponseWriter, r *http.Request) {

	//OSCPtoken = r.Header.Get("Authorization")
	//fmt.Println(r.Header.Get("Authorization"))
	OSCPtoken := r.Header.Get("Authorization")
	vars := mux.Vars(r)
	key := vars["GROUP"]

	var newgroup Group
	_ = json.NewDecoder(r.Body).Decode(&newgroup)
	fmt.Println("newgroup:>", newgroup)

	req, err := http.NewRequest("GET", OSCPBaseURL+"/oapi/v1/groups/"+key, nil)
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)

	var osgroup OSGroup
	json.Unmarshal([]byte(string(body)), &osgroup)
	fmt.Println("osgroup:>", osgroup)

	for _, usertodelete := range newgroup.Users {
		for i1, v := range osgroup.Users {
			if v == usertodelete {
				//fmt.Println("found", v)
				osgroup.Users = append(osgroup.Users[:i1], osgroup.Users[i1+1:]...)
				//fmt.Println("osgroup after deletion:>", osgroup)
			}
		}
	}

	jsonStr, err := json.Marshal(osgroup)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(jsonStr))

	req, err = http.NewRequest("PUT", OSCPBaseURL+"/oapi/v1/groups/"+key, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", OSCPtoken)

	if err != nil {
		fmt.Println(err)
		return
	}

	client = &http.Client{}
	resp, err = client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.StatusCode)
	//fmt.Println("response Headers:", resp.Header)
	body, _ = ioutil.ReadAll(resp.Body)

	//fmt.Println("response Body:", string(body))

	//w.WriteHeader(resp.StatusCode)
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		w.WriteHeader(resp.StatusCode)

	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)

}